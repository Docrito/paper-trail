extends Camera2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func transition_done():
	get_node("../../").transition()
	pass
	
func transition_right_begin():
	get_node("../../").transition_right_begin()
	
func transition_right():
	get_node("../../").transition_right()
	
func transition_left_begin():
	get_node("../../").transition_left_begin()
	
func transition_left():
	get_node("../../").transition_left()