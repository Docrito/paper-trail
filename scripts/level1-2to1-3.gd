extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	connect("body_entered",self,"_on_Area2D_body_enter")
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _on_Area2D_body_enter( body ):
	call_deferred("set_enable_monitoring", false)
	get_node("../../").next_left_scene_path = "res://scenes/test.tscn"
	get_node("../../").next_left_scene = load("res://scenes/test.tscn")
	get_node("../../").next_left_scene_position = Vector2(0, 0)
	
	get_node("../../").next_right_scene_path = "res://scenes/level1-4.tscn"
	get_node("../../").next_right_scene = load("res://scenes/level1-4.tscn")
	get_node("../../").next_right_scene_position = Vector2(1152, 0)
	
	get_node("../camera/AnimationPlayer").play("transition_right")