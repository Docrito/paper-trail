extends StaticBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	get_node("Area2D").connect("body_entered",self,"_on_Area2D_body_enter")
	get_node("Area2D").connect("body_exited",self,"_on_Area2D_body_exit")
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _on_Area2D_body_enter( body ):
    if body.has_method("blow"): body.blow(rotation_degrees)

func _on_Area2D_body_exit( body ):
    if body.has_method("stop"): body.stop()